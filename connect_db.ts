import mongoose, { Model, Document } from "mongoose";
import { config } from "dotenv";
config()


export async function connect_db() {
  console.debug('Connecting to database')
  return mongoose.connect(process.env.DB_URL as string)
    .catch(err => {
      console.error('*'.repeat(30))
      console.error(err)
      console.error('*'.repeat(30))
      process.exit(1)
    })
}

interface IChar extends Document { char: string, value: number }

const schema = new mongoose.Schema({
  char: { type: String, index: true, unique: true },
  value: { type: Number }
})

const char_model: Model<IChar> = mongoose.model('chars', schema)

export async function get_character_map() {
  return char_model.find()
    .then(docs => {
      const map = {} as { [key: string]: number }
      docs.forEach(({ char, value }) => {
        map[char] = value
      })
      return map
    })
}

export default char_model
