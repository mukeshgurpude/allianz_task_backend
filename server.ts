import express from 'express'
import cors from 'cors'
import { config } from 'dotenv'
import { connect_db, get_character_map } from './connect_db'
config()

const app = express()
app.use(cors({ origin: '*' }))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const allowed_chars = '*/+-<>'

app.post('/calculate', async function(req, res) {
  const expression: string[] = req.body
  const map = await get_character_map()

  if (expression.length < 3) return res.send('Expression must be atleast 3 characters')
  const rhs = Number(expression.at(-1))
  const condition = expression.at(-2)

  if (Number.isNaN(rhs)) return res.send('RHS must be a number')
  if (!['<', '>'].includes(condition as string)) return res.send('Number must be followed by a condition')

  let parsed = ''
  // For each char in expression, if char in map, add to parsed, else add to parsed as is
  for (let i = 0; i < expression.length - 1; i++) {
    // @ts-ignore
    if (map[expression[i]]) parsed += map[expression[i]]
    else if (allowed_chars.includes(expression[i])) {
      parsed += expression[i]
    } else {
      return res.send('Invalid character')
    }
  }
  parsed += rhs
  const answer = eval(parsed)

  res.send(answer)
})

connect_db()
  .then(() => {
    app.listen(process.env.PORT || 8000, function() {
      console.debug(`Server started on ${process.env.PORT || 8000}`)
    })
  })
